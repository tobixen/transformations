def to_new_zero(angle, skew):
    return angle-skew

def from_new_zero(angle, skew):
    return angle+skew

def transform(angle, prev_min, new_min, prev_max, new_max):
    factor = (new_max-new_min)/(prev_max-prev_min)
    return from_new_zero(to_new_zero(angle, prev_min)*factor, new_min)

