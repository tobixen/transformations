from unittest import TestCase
from math import pi
from transform import to_new_zero, from_new_zero, transform

class TestTransform(TestCase):
    def test_skew_unskew(self):
        for angle in (0, pi, pi/2, 2*pi, 233):
            for skew in (0, pi, pi/2, 2*pi, 233):
                self.assertAlmostEqual(from_new_zero(to_new_zero(angle, skew), skew), angle)
                
    def test_transform(self):
        self.assertAlmostEqual(transform(0, new_min=0, new_max=1, prev_min=0, prev_max=1), 0)
        self.assertAlmostEqual(transform(0.5, new_min=0, new_max=1, prev_min=0, prev_max=1), 0.5)
        self.assertAlmostEqual(transform(1, new_min=0, new_max=1, prev_min=0, prev_max=1), 1)
        self.assertAlmostEqual(transform(1, new_min=0, new_max=10, prev_min=0, prev_max=1), 10)
        self.assertAlmostEqual(transform(0, new_min=0, new_max=10, prev_min=0, prev_max=1), 0)
        self.assertAlmostEqual(transform(1, new_min=1, new_max=10, prev_min=1, prev_max=2), 1)
        self.assertAlmostEqual(transform(2, new_min=1, new_max=10, prev_min=1, prev_max=2), 10)
        self.assertAlmostEqual(transform(1, new_min=2, new_max=10, prev_min=1, prev_max=2), 2)
        self.assertAlmostEqual(transform(2, new_min=2, new_max=10, prev_min=1, prev_max=2), 10)

        self.assertAlmostEqual(transform(1.5 * pi, new_min=1*pi, new_max=2.0*pi, prev_min=1.5*pi, prev_max=2.0*pi), pi)
        self.assertAlmostEqual(transform(2 * pi,  new_min=1*pi, new_max=2.0*pi, prev_min=1.5*pi, prev_max=2.0*pi), 2*pi)

